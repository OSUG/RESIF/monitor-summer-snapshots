FROM pfeiffermax/python-poetry:1.2.0-poetry1.4.1-python3.10.10-slim-bullseye
WORKDIR /app
COPY poetry.lock pyproject.toml /app
# Project initialization:
COPY . /app
RUN poetry config virtualenvs.create false \
  && poetry install --only main --no-interaction --no-ansi
# Creating folders, and files for a project:
CMD mosusnap
