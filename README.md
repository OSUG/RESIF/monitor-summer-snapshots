# monitor-summer-snapshots

![](screenshot.png)

## Installation

monitor-summer-snapshots is distributed on the local gitlab package registry as a
universal wheel and is available on Linux/macOS and Windows and supports
Python 3.7.

``` bash
$ pip install monitor-summer-snapshots  --index-url https://gricad-gitlab.univ-grenoble-alpes.fr/api/v4/projects/1774/packages/pypi/simple
```

To see the list of installed files :

``` bash
$ pip show -f monitoring-summer-snapshots
```

## License

monitor-summer-snapshots is distributed under the terms of the GPLv3,
see LICENSE file.


## Configuration

### Monitoring

Copier le fichier `config.yml.example` en `config.yml` et l'éditer :

    influxdb:                        # Paramétrage de l'envoi à influxdb
      active: true                   # placer à false pour désactiver l'envoi à influxdb
      server: plop.u-ga.fr           # le serveur influxdb
      port: 8086                     # le port
      database: plop                 # le nom de la base de donnée
      measurement: summer_snapshot   # le nom de la mesure
    zabbix:
      active: true                   # placer à false pour désactiver l'envoi à zabbix
      server: plop.u-ga.fr           # serveur zabbix
      host: thisone                  # nom de l'hote qui envoie la métrique
      key: summer.snapshot.size      # la clé de l'item telle qu'elle est configurée dans zabbix

    lifs:                            # liste des LIF SUMMER
      - name: resif                  # un nom pour cette LIF
        sshserver:                   # le serveur SSH d'administration
        login:                       # le login SSH
        password:                    # devine
        volumes:                     # une liste des volumes à monitorer
          - name:                    # nom du volume dans la LIF
            mountpoint:              # un nom logique du volume pour le monitoring

Les logs se configurent dans le fichier de logs `logger.conf`.

## Zabbix

Il faut configurer zabbix pour qu'il accueille les métriques remontées
par le script

#### Template

Il faut importer le template proposé dans ce dépôt
(<https://gricad-gitlab.univ-grenoble-alpes.fr/schaeffj/monitoring-summer-snapshot/blob/master/monitor_summer_snapshots/zabbix_template.xml>)
et éventuellement l'adapter à vos besoin.

Assigner ce template au serveur sur lequel est déployé monitor-summer-snapshots.

#### Discovery

Zabbix va découvrir par lui-même la liste des volumes SUMMER, en fonction des lifs déclarées dans le fichier de configuration.

Après le déploiement de l'application, configurer l'agent zabbix, par
exemple `/etc/zabbix/zabbix_agent.d/summer-snapshots-discovery.conf`

```
UserParameter=summer_snapshots_mount.discovery,MOSUSNAP_CONFIG_FILE=/home/sysop/.config/mosusnap.yaml /home/sysop/.local/bin/mosusnap_zbx_lld
```

Et redémarrer l'agent.

``` bash
systemctl restart zabbix-agent
```

## Influxdb

Pour utiliser l'output influxdb, il faut créer une base de données et donner des droits `write` à un utilisateur qui sera configuré pour mosusnap.

## Lancement

Le fichier de configuration peut être passé en variable d'environnement:

    MOSUSNAP_CONFIG_FILE=~/.config/mosusnap.yaml mosusnap
    
On peut lancer l'application quotidiennement par exemple avec un cronjob.

