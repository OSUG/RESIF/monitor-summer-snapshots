from influxdb import InfluxDBClient
import pprint

pp = pprint.PrettyPrinter(indent=4)

client = InfluxDBClient(host='osug-influxdb.u-ga.fr', database='telegrafresif', username='admin', password='UL2Pku8GqSPVHgvwvR', ssl=True, verify_ssl=False)
db_data = client.query("select * from forever.summervolumes where volume = 'test_data' and lif='resif'")

data_to_write=[]
for d in db_data.get_points():
    # {   'available': 6992893952655,
    # 'lif': 'resif',
    # 'qtree': None,
    # 'qtree_limit': None,
    # 'qtree_used': None,
    # 'snapshot_reserve': 934584883609,
    # 'snapshot_used': 1979120929996,
    # 'time': '2021-08-28T08:00:36Z',
    # 'total': 18691697672192,
    # 'used': 10764218835927,
    # 'volume': 'bud_data'}

    if d['available']:
        available = int(d['available'])
    else:
        available = None
    data_to_write.append(
        {
            'measurement': 'summervolumes',
            'tags': {'lif': d['lif'],
                'volume': 'test',
                'qtree': d['qtree'] },
            'time': d['time'],
            'fields': {'used': int(d['used']), 'total': int(d['total']), 'available': available}
        })

pp.pprint(data_to_write)
client.write_points(data_to_write, retention_policy='forever')
